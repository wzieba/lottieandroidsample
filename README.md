# Lottie lib example

* Library: [https://airbnb.design/lottie/](https://airbnb.design/lottie/)
* Animation used: [https://www.lottiefiles.com/427-happy-birthday](https://www.lottiefiles.com/427-happy-birthday)

## Sample
![Demo gif](https://bytebucket.org/wzieba/lottieandroidsample/raw/a91e75e8e81ee641321ab9b8c3b19708b6870ae1/raw/demo.gif)